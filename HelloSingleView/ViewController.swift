//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Sam Zammit on 5/12/19.
//  Copyright © 2019 Sam Zammit. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func touchUpInsideFunc(_ sender: Any) {
        print("Hello button")
    }
    
}

